﻿/// https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test [04-05-2018]
using System;

namespace Prime.Services
{
    public class PrimeService
    {
        public bool IsPrime(int candidate)
        {
            if (candidate < 2)
            {
                return false;
            }
            throw new NotImplementedException("Please create a test first.");
        }
    }
}
