/// This tutorial takes you through an interactive experience building a sample solution
/// step-by-step to learn unit testing concepts. If you prefer to follow the tutorial using
/// a pre-built solution, view or download the sample code before you begin. For download 
/// instructions, see Samples and Tutorials.
///
/// https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-mstest
/// [04-05-2018]
///
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Prime.Services;

namespace Prime.UnitTests.Services
{
    [TestClass]
    public class PrimeService_IsPrimeShould
    {
        private readonly PrimeService _primeService;

        public PrimeService_IsPrimeShould()
        {
            _primeService = new PrimeService();
        }

        [TestMethod]
        public void ReturnFalseGivenValueOf1()
        {
            var result = _primeService.IsPrime(1);

            Assert.IsFalse(result, "1 should not be prime.");
        }

        [DataTestMethod]
        [DataRow(-1)]
        [DataRow(0)]
        [DataRow(1)]
        public void ReturnFalseGivenValueLessThan2(int value)
        {
            var result = _primeService.IsPrime(value);

            Assert.IsFalse(result, $"{value} should not be prime.");

            Assert.IsFalse(result, "1 should not be prime.");
        }
    }
}
