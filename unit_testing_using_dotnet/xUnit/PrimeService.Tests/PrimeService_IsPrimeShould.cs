/// This tutorial takes you through an interactive experience building a sample solution step-by-step to learn 
/// unit testing concepts. If you prefer to follow the tutorial using a pre-built solution, view or download
/// the sample code before you begin. For download instructions, see Samples and Tutorials.
///
/// https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test [04-05-2018]
using Xunit;
using Prime.Services;

namespace Prime.UnitTests.Services
{
    public class PrimeService_IsPrimeShould
    {
      private readonly PrimeService _primeService;

      public PrimeService_IsPrimeShould()
      {
        _primeService = new PrimeService();
      }

      [Fact]
      public void ReturnFalseGivenValueOf1()
      {
        var result = _primeService.IsPrime(1);

        Assert.False(result, "1 should not be prime");
      }

      [Theory]
      [InlineData(-1)]
      [InlineData(0)]
      [InlineData(1)]
      public void ReturnFalseGivenValueLessThan2(int value)
      {
        var result = _primeService.IsPrime(value);

        Assert.False(result, $"{value} should not be prime");
      }

    }
}