/// This tutorial takes you through an interactive experience building a sample solution step-by-step
/// to learn unit testing concepts. If you prefer to follow the tutorial using a pre-built solution, 
/// view or download the sample code before you begin. For download instructions, see Samples and Tutorials.
///
/// https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-nunit [04-05-1983]
using NUnit.Framework;
using Prime.Services;

namespace Prime.UnitTests.Services
{
    [TestFixture]
    public class PrimeService_IsPrimeShould
    {
        private readonly PrimeService _primeService;

        public PrimeService_IsPrimeShould()
        {
            _primeService = new PrimeService();
        }

        [Test]
        public void ReturnFalseGivenValueOf1()
        {
            var result = _primeService.IsPrime(1);

            Assert.IsFalse(result, "1 should not be prime.");
        }

        [TestCase(-1)]
        [TestCase(0)]
        [TestCase(1)]
        public void ReturnFalseGivenValuesLessThan2(int value)
        {
            var result = _primeService.IsPrime(value);

            Assert.IsFalse(result, $"{value} should not be prime.");
        }
    }
}