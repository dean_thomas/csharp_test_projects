﻿using System;
using System.Linq;
using static Functional.Functional;

namespace ConsoleApp1
{
    class Program
    {
		static void Main(string[] args)
        {
			//	Usage:
			//		Unfold(seed, predicate, operator)
			var basic = Unfold(0, n => n < 10, n => n + 1);
			var fibonacci = Unfold((0, 1), f => f.Item2 < 1000, f => (f.Item2, f.Item1 + f.Item2));

			basic.ToList().ForEach(t => Console.WriteLine(t));
			Console.WriteLine();
			fibonacci.ToList().ForEach(t => Console.WriteLine(t.Item2));

			Console.ReadLine();
		}
    }
}
