﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Functional
{
    public static class Functional
    {
		public static IEnumerable<T> Unfold<T>(T seed, Func<T, bool> pred, Func<T, T> op)
		{
			Func<T, Tuple<T, T>> wrap(Func<T, bool> p) => (state) => p(state) ? Tuple.Create(state, op(state)) : null;

			var func = wrap(pred);
			var a = func(seed);
			if (a == null)
				return new List<T>();
			else
				return new List<T> { a.Item1 }.Concat(Unfold(a.Item2, pred, op));
		}
	}
}
