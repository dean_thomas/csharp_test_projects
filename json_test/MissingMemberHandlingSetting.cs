﻿using Newtonsoft.Json;
using System;

namespace json_test
{
	internal class MissingMemberHandlingSetting
	{
		public class Account
		{
			public string FullName { get; set; }
			public bool Deleted { get; set; }
		}

		public MissingMemberHandlingSetting()
		{
			string json = @"{
  'FullName': 'Dan Deleted',
  'Deleted': true,
  'DeletedDate': '2013-01-20T00:00:00'
}";

			try
			{
				JsonConvert.DeserializeObject<Account>(json, new JsonSerializerSettings
				{
					MissingMemberHandling = MissingMemberHandling.Error
				});
			}
			catch (JsonSerializationException ex)
			{
				Console.WriteLine(ex.Message);
				// Could not find member 'DeletedDate' on object of type 'Account'. Path 'DeletedDate', line 4, position 23.
			}
		}
	}
}