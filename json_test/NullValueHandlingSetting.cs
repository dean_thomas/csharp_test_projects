﻿using Newtonsoft.Json;
using System;

/// <summary>
/// This sample serializes an object to JSON with NullValueHandling set to Ignore so that properties with a default value aren't included in the JSON result.
/// https://www.newtonsoft.com/json/help/html/NullValueHandlingIgnore.htm
/// [19-04-2018]
/// </summary>
namespace json_test
{
	internal class NullValueHandlingSetting
	{
		public class Person
		{
			public string Name { get; set; }
			public int Age { get; set; }
			public Person Partner { get; set; }
			public decimal? Salary { get; set; }
		}

		public NullValueHandlingSetting()
		{
			Person person = new Person
			{
				Name = "Nigal Newborn",
				Age = 1
			};

			string jsonIncludeNullValues = JsonConvert.SerializeObject(person, Formatting.Indented);

			Console.WriteLine(jsonIncludeNullValues);
			// {
			//   "Name": "Nigal Newborn",
			//   "Age": 1,
			//   "Partner": null,
			//   "Salary": null
			// }

			string jsonIgnoreNullValues = JsonConvert.SerializeObject(person, Formatting.Indented, new JsonSerializerSettings
			{
				NullValueHandling = NullValueHandling.Ignore
			});

			Console.WriteLine(jsonIgnoreNullValues);
			// {
			//   "Name": "Nigal Newborn",
			//   "Age": 1
			// }
		}
	}
}