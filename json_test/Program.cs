﻿using json_test.AnnotationExperiments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace json_test
{
	/// <summary>
	/// https://www.newtonsoft.com/json/help/html/Samples.htm
	/// [11-04-2018]
	/// </summary>
	class Program
	{
		static void Main(string[] args)
		{
			//new BasicSerializationTest();
			//new SerializeACollectionTest();
			//new SerializeADictionaryTest();
			//new SerializeJsonToAFileTest();
			//new SerializeWithJsonConvertersTest();
			//new SerializeADataSetTest();
			//new SerializeRawJsonValue();
			//new SerializeUnindentedJson();
			//new SerializeConditionalProperty();

			//new DeserializeAnObject();
			//new DeserializeACollection();
			//new DeserializeADictionary();
			//new DeserializeAnAnonymousType();
			//new DeserializeADataSet();
			//new DeserializeWithCustomCreationConverter();
			//new DeserializeJsonFromAFile();

			//new PopulateAnObject();

			//new ConstructorHandlingSetting();
			//new DefaultValueHandlingSetting();
			//new MissingMemberHandlingSetting();
			//new NullValueHandlingSetting();
			//new ReferenceLoopHandlingSetting();
			//new PreserveReferencesHandlingSetting();
			//new DateFormatHandlingSetting();
			//new DateTimeZoneHandlingSetting();
			//new TypeNameHandlingSetting();
			//new MetadataPropertyHandlingSetting();

			new AnnotationExperiment();

			Console.WriteLine("Press any key.");
			Console.ReadKey();
		}
	}
}
