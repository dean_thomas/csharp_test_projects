﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace json_test
{
	/// <summary>
	/// This sample deserializes JSON retrieved from a file.
	/// https://www.newtonsoft.com/json/help/html/DeserializeWithJsonSerializerFromFile.htm
	/// [18-04-2018]
	/// </summary>
	internal class DeserializeJsonFromAFile
	{
		public class Movie
		{
			public string Name { get; set; }
			public int Year { get; set; }
		}

		public DeserializeJsonFromAFile()
		{
			var desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

			// read file into a string and deserialize JSON to a type
			Movie movie1 = JsonConvert.DeserializeObject<Movie>(File.ReadAllText($"{desktopPath}\\test1.json"));

			// deserialize JSON directly from a file
			using (StreamReader file = File.OpenText($"{desktopPath}\\test1.json"))
			{
				JsonSerializer serializer = new JsonSerializer();
				Movie movie2 = (Movie)serializer.Deserialize(file, typeof(Movie));
			}
		}
	}
}