﻿using Newtonsoft.Json;
using System;

namespace json_test
{
	internal class DeserializeAnAnonymousType
	{
		/// <summary>
		/// This sample deserializes JSON into an anonymous type.
		/// https://www.newtonsoft.com/json/help/html/DeserializeAnonymousType.htm
		/// [13-04-2018]
		/// </summary>
		public DeserializeAnAnonymousType()
		{
			var definition = new { Name = "" };

			string json1 = @"{'Name':'James'}";
			var customer1 = JsonConvert.DeserializeAnonymousType(json1, definition);

			Console.WriteLine(customer1.Name);
			// James

			string json2 = @"{'Name':'Mike'}";
			var customer2 = JsonConvert.DeserializeAnonymousType(json2, definition);

			Console.WriteLine(customer2.Name);
			// Mike
		}
	}
}