﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace json_test
{
	/// <summary>
	/// This sample deserializes JSON to an object.
	/// https://www.newtonsoft.com/json/help/html/DeserializeObject.htm
	/// [13-04-2018]
	/// </summary>
	internal class DeserializeAnObject
	{
		public class Account
		{
			public string Email { get; set; }
			public bool Active { get; set; }
			public DateTime CreatedDate { get; set; }
			public IList<string> Roles { get; set; }
		}

		public DeserializeAnObject()
		{
			string json = @"{
  'Email': 'james@example.com',
  'Active': true,
  'CreatedDate': '2013-01-20T00:00:00Z',
  'Roles': [
    'User',
    'Admin'
  ]
}";
			Account account = JsonConvert.DeserializeObject<Account>(json);

			Console.WriteLine(account.Email);
			// james@example.com
		}
	}
}