﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace json_test
{
	/// <summary>
	/// This sample creates a class that inherits from CustomCreationConverter<T> that instantiates
	/// Employee instances for the Person type.
	/// https://www.newtonsoft.com/json/help/html/DeserializeCustomCreationConverter.htm
	/// [18-04-2018]
	/// </summary>
	public class Person
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public DateTime BirthDate { get; set; }
	}

	public class Employee : Person
	{
		public string Department { get; set; }
		public string JobTitle { get; set; }
	}

	public class PersonConverter : CustomCreationConverter<Person>
	{
		public override Person Create(Type objectType)
		{
			return new Employee();
		}
	}

	public class DeserializeWithCustomCreationConverter
	{
		public DeserializeWithCustomCreationConverter()
		{
			string json = @"{
  'Department': 'Furniture',
  'JobTitle': 'Carpenter',
  'FirstName': 'John',
  'LastName': 'Joinery',
  'BirthDate': '1983-02-02T00:00:00'
}";

			Person person = JsonConvert.DeserializeObject<Person>(json, new PersonConverter());

			Console.WriteLine(person.GetType().Name);
			// Employee

			Employee employee = (Employee)person;

			Console.WriteLine(employee.JobTitle);
			// Carpenter
		}
	}
}
