﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace json_test
{
	/// <summary>
	/// This sample deserializes JSON into a dictionary.
	/// https://www.newtonsoft.com/json/help/html/DeserializeDictionary.htm
	/// [13-04-2018]
	/// </summary>
	internal class DeserializeADictionary
	{
		public DeserializeADictionary()
		{
			string json = @"{
  'href': '/account/login.aspx',
  'target': '_blank'
}";

			Dictionary<string, string> htmlAttributes = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

			Console.WriteLine(htmlAttributes["href"]);
			// /account/login.aspx

			Console.WriteLine(htmlAttributes["target"]);
			// _blank
		}
	}
}