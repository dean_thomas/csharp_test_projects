﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace json_test
{
	/// <summary>
	/// This sample deserializes JSON into a collection.
	/// https://www.newtonsoft.com/json/help/html/DeserializeCollection.htm
	/// [13-04-2018]
	/// </summary>
	internal class DeserializeACollection
	{
		public DeserializeACollection()
		{
			string json = @"['Starcraft','Halo','Legend of Zelda']";

			List<string> videogames = JsonConvert.DeserializeObject<List<string>>(json);

			Console.WriteLine(string.Join(", ", videogames.ToArray()));
			// Starcraft, Halo, Legend of Zelda
		}
	}
}