﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// This sample populates an existing object instance with values from JSON.
/// https://www.newtonsoft.com/json/help/html/PopulateObject.htm
/// [19-04-2018]
/// </summary>
namespace json_test
{
	public class Account
	{
		public string Email { get; set; }
		public bool Active { get; set; }
		public DateTime CreatedDate { get; set; }
		public List<string> Roles { get; set; }
	}

	public class PopulateAnObject
	{
		public PopulateAnObject()
		{
			Account account = new Account
			{
				Email = "james@example.com",
				Active = true,
				CreatedDate = new DateTime(2013, 1, 20, 0, 0, 0, DateTimeKind.Utc),
				Roles = new List<string>
	{
		"User",
		"Admin"
	}
			};

			string json = @"{
  'Active': false,
  'Roles': [
    'Expired'
  ]
}";

			JsonConvert.PopulateObject(json, account);

			Console.WriteLine(account.Email);
			// james@example.com

			Console.WriteLine(account.Active);
			// false

			Console.WriteLine(string.Join(", ", account.Roles.ToArray()));
			// User, Admin, Expired
		}
	}
}
