﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// This sample uses the DefaultValueHandling setting to not serialize properties with a default value.
/// https://www.newtonsoft.com/json/help/html/DefaultValueHandlingIgnore.htm
/// [19-04-2018]
/// </summary>
namespace json_test
{
	public class UserViewModel
	{
		public string Name { get; set; }
		public IList<string> Offices { get; private set; }

		public UserViewModel()
		{
			Offices = new List<string>
		{
			"Auckland",
			"Wellington",
			"Christchurch"
		};
		}
	}

	public class DefaultValueHandlingSetting
	{
		public DefaultValueHandlingSetting()
		{
			string json = @"{
  'Name': 'James',
  'Offices': [
    'Auckland',
    'Wellington',
    'Christchurch'
  ]
}";

			UserViewModel model1 = JsonConvert.DeserializeObject<UserViewModel>(json);

			foreach (string office in model1.Offices)
			{
				Console.WriteLine(office);
			}
			// Auckland
			// Wellington
			// Christchurch
			// Auckland
			// Wellington
			// Christchurch

			UserViewModel model2 = JsonConvert.DeserializeObject<UserViewModel>(json, new JsonSerializerSettings
			{
				ObjectCreationHandling = ObjectCreationHandling.Replace
			});

			foreach (string office in model2.Offices)
			{
				Console.WriteLine(office);
			}
			// Auckland
			// Wellington
			// Christchurch
		}
	}
}
