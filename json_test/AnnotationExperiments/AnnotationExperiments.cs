﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace json_test.AnnotationExperiments
{
	public class MyClass1
	{
		[JsonProperty("value_string")]
		public string ValueString { get; set; }
		[JsonProperty("value_nullable_int")]
		public int? ValueNullableInt { get; set; }
		[JsonProperty("value_int")]
		public int ValueInt { get; set; }
		[JsonProperty("value_default_int")]
		public int DefaultInt { get; set; }
		//public int RequiredInt 
	}

	public class MyClass2
	{
		[JsonProperty("value_string", NullValueHandling = NullValueHandling.Ignore)]
		public string ValueString { get; set; }
		[JsonProperty("value_nullable_int", NullValueHandling = NullValueHandling.Ignore)]
		public int? ValueNullableInt { get; set; }
		[JsonProperty("value_int")]
		public int ValueInt { get; set; }
		[JsonProperty("value_default_int", DefaultValueHandling = DefaultValueHandling.Ignore)]
		public int DefaultInt { get; set; }
		[JsonProperty("value_required_int", Required = Required.Always)]
		public int RequiredInt { get; set; }
	}

	public class AnnotationExperiment
	{
		public AnnotationExperiment()
		{
			var objects = new List<object>
			{
				new MyClass1 { ValueString = "Test1", ValueInt = 42, ValueNullableInt = 666, DefaultInt = 0 },
				new MyClass1 { ValueInt = 500 },

				new MyClass2 { ValueString = "Test1", ValueInt = 42, ValueNullableInt = 666, DefaultInt = 0, RequiredInt = 10 },
				new MyClass2 { ValueInt = 500, RequiredInt =88 },
			};

			//	Run through the lsit and serialize each object
			objects.ForEach(t => Console.WriteLine(JsonConvert.SerializeObject(t).ToString()));

			//	Convert to Object1 - this should work fine
			var x = objects.Select(t => JsonConvert.DeserializeObject<MyClass1>(JsonConvert.SerializeObject(t))).ToList();
			x.ForEach(t => Console.WriteLine(JsonConvert.SerializeObject(t)));

			try
			{
				//	Convert to Object2 - this should fail as MyClass1 objects will be lacking the RequiredInt field 
				var y = objects.Select(t => JsonConvert.DeserializeObject<MyClass2>(JsonConvert.SerializeObject(t))).ToList();
				y.ForEach(t => Console.WriteLine(JsonConvert.SerializeObject(t)));
			}
			catch (JsonSerializationException ex)
			{
				Console.WriteLine($"Was unable to convert object to 'MyClass2': {ex.Message}");
			}
		}
	}
}
