﻿using Newtonsoft.Json;
using System;

namespace json_test
{
	/// <summary>
	/// This sample uses a conditional property to exclude a property from serialization.
	/// https://www.newtonsoft.com/json/help/html/SerializeConditionalProperty.htm
	/// [12-04-2018]
	/// </summary>
	internal class SerializeConditionalProperty
	{
		public class Employee
		{
			public string Name { get; set; }
			public Employee Manager { get; set; }

			public bool ShouldSerializeManager()
			{
				// don't serialize the Manager property if an employee is their own manager
				return (Manager != this);
			}
		}

		public SerializeConditionalProperty()
		{
			Employee joe = new Employee();
			joe.Name = "Joe Employee";
			Employee mike = new Employee();
			mike.Name = "Mike Manager";

			joe.Manager = mike;

			// mike is his own manager
			// ShouldSerialize will skip this property
			mike.Manager = mike;

			string json = JsonConvert.SerializeObject(new[] { joe, mike }, Formatting.Indented);

			Console.WriteLine(json);
			// [
			//   {
			//     "Name": "Joe Employee",
			//     "Manager": {
			//       "Name": "Mike Manager"
			//     }
			//   },
			//   {
			//     "Name": "Mike Manager"
			//   }
			// ]
		}
	}
}