﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace json_test
{
	/// <summary>
	/// This sample uses JRaw properties to serialize JSON with raw content.
	/// https://www.newtonsoft.com/json/help/html/SerializeRawJson.htm
	/// [12-04-2018]
	/// </summary>
	internal class SerializeRawJsonValue
	{
		public class JavaScriptSettings
		{
			public JRaw OnLoadFunction { get; set; }
			public JRaw OnUnloadFunction { get; set; }
		}
	
		public SerializeRawJsonValue()
		{
			JavaScriptSettings settings = new JavaScriptSettings
			{
				OnLoadFunction = new JRaw("OnLoad"),
				OnUnloadFunction = new JRaw("function(e) { alert(e); }")
			};

			string json = JsonConvert.SerializeObject(settings, Formatting.Indented);

			Console.WriteLine(json);
			// {
			//   "OnLoadFunction": OnLoad,
			//   "OnUnloadFunction": function(e) { alert(e); }
			// }
		}
	}
}