﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace json_test
{
	/// <summary>
	/// This sample serializes an object to JSON.
	/// https://www.newtonsoft.com/json/help/html/SerializeObject.htm
	/// [11-04-2018]
	/// </summary>
	public class BasicSerializationTest
	{
		public class Account
		{
			public string Email { get; set; }
			public bool Active { get; set; }
			public DateTime CreatedDate { get; set; }
			public IList<string> Roles { get; set; }
		}

		public BasicSerializationTest()
		{

			Account account = new Account
			{
				Email = "james@example.com",
				Active = true,
				CreatedDate = new DateTime(2013, 1, 20, 0, 0, 0, DateTimeKind.Utc),
				Roles = new List<string>
				{
					"User",
					"Admin"
				}
			};

			string json = JsonConvert.SerializeObject(account, Formatting.Indented);
			Console.WriteLine(json);
		}
	}
}
