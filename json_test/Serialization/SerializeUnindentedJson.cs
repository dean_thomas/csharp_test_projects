﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace json_test
{
	/// <summary>
	/// This sample serializes an object to JSON without any formatting or indentation whitespace.
	/// https://www.newtonsoft.com/json/help/html/SerializeUnindentedJson.htm
	/// [12-04-2018]
	/// </summary>
	internal class SerializeUnindentedJson
	{
		public class Account
		{
			public string Email { get; set; }
			public bool Active { get; set; }
			public DateTime CreatedDate { get; set; }
			public IList<string> Roles { get; set; }
		}

		public SerializeUnindentedJson()
		{
			Account account = new Account
			{
				Email = "james@example.com",
				Active = true,
				CreatedDate = new DateTime(2013, 1, 20, 0, 0, 0, DateTimeKind.Utc),
				Roles = new List<string>
				{
					"User",
					"Admin"
				}
			};

			string json = JsonConvert.SerializeObject(account);
			// {"Email":"james@example.com","Active":true,"CreatedDate":"2013-01-20T00:00:00Z","Roles":["User","Admin"]}

			Console.WriteLine(json);
		}
	}
}