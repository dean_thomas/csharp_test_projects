﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace json_test
{
	public class SerializeADictionaryTest
	{
		/// <summary>
		/// This sample serializes a dictionary to JSON.
		/// https://www.newtonsoft.com/json/help/html/SerializeDictionary.htm
		/// [11-04-2018]
		/// </summary>
		public SerializeADictionaryTest()
		{
			Dictionary<string, int> points = new Dictionary<string, int>
			{
				{ "James", 9001 },
				{ "Jo", 3474 },
				{ "Jess", 11926 }
			};

			string json = JsonConvert.SerializeObject(points, Formatting.Indented);

			Console.WriteLine(json);
		}
	}
}
