﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace json_test
{
	/// <summary>
	/// This sample serializes JSON to a file.
	/// https://www.newtonsoft.com/json/help/html/SerializeWithJsonSerializerToFile.htm
	/// [11-04-2018]
	/// </summary>
	internal class SerializeJsonToAFileTest
	{
		public class Movie
		{
			public string Name { get; set; }
			public int Year { get; set; }
		}

		public SerializeJsonToAFileTest()
		{
			Movie movie = new Movie
			{
				Name = "Bad Boys",
				Year = 1995
			};

			var desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

			// serialize JSON to a string and then write string to a file
			File.WriteAllText($"{desktopPath}\\test1.json", JsonConvert.SerializeObject(movie));

			// serialize JSON directly to a file
			using (StreamWriter file = File.CreateText($"{desktopPath}\\test2.json"))
			{
				JsonSerializer serializer = new JsonSerializer();
				serializer.Serialize(file, movie);
			}
		}
	}
}