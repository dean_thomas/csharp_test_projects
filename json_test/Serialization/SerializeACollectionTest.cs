﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace json_test
{
	public class SerializeACollectionTest
	{
		/// <summary>
		/// This sample serializes a collection to JSON.
		/// https://www.newtonsoft.com/json/help/html/SerializeCollection.htm
		/// [11-04-2018]
		/// </summary>
		public SerializeACollectionTest()
		{
			List<string> videogames = new List<string>
			{
				"Starcraft",
				"Halo",
				"Legend of Zelda"
			};

			string json = JsonConvert.SerializeObject(videogames, Formatting.Indented);

			Console.WriteLine(json);
		}
	}
}
