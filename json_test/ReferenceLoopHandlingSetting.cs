﻿using Newtonsoft.Json;
using System;

namespace json_test
{
	/// <summary>
	/// This sample sets ReferenceLoopHandling to Ignore so that looping values are excluded from serialization instead of throwing an exception.
	/// https://www.newtonsoft.com/json/help/html/ReferenceLoopHandlingIgnore.htm
	/// [20-04-2018]
	/// </summary>
	internal class ReferenceLoopHandlingSetting
	{
		public class Employee
		{
			public string Name { get; set; }
			public Employee Manager { get; set; }
		}

		public ReferenceLoopHandlingSetting()
		{
			Employee joe = new Employee { Name = "Joe User" };
			Employee mike = new Employee { Name = "Mike Manager" };
			joe.Manager = mike;
			mike.Manager = mike;

			string json = JsonConvert.SerializeObject(joe, Formatting.Indented, new JsonSerializerSettings
			{
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore
			});

			Console.WriteLine(json);
			// {
			//   "Name": "Joe User",
			//   "Manager": {
			//     "Name": "Mike Manager"
			//   }
			// }
		}
	}
}