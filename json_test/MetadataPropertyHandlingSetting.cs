﻿using Newtonsoft.Json;
using System;

namespace json_test
{
	/// <summary>
	/// This sample deserializes JSON with MetadataPropertyHandling set to ReadAhead 
	/// so that metadata properties do not need to be at the start of an object.
	/// https://www.newtonsoft.com/json/help/html/DeserializeMetadataPropertyHandling.htm
	/// [20-04-2018]
	/// </summary>
	internal class MetadataPropertyHandlingSetting
	{
		public class User
		{
			public string Name;
		}

		public MetadataPropertyHandlingSetting()
		{
			string json = @"{
			  'Name': 'James',
			  'Password': 'Password1',
			  '$type': 'MetadataPropertyHandlingSetting.User, json_test'
			}";

			object o = JsonConvert.DeserializeObject(json, new JsonSerializerSettings
			{
				TypeNameHandling = TypeNameHandling.All,
				// $type no longer needs to be first
				MetadataPropertyHandling = MetadataPropertyHandling.ReadAhead
			});

			User u = (User)o;

			Console.WriteLine(u.Name);
			// James
		}
	}
}