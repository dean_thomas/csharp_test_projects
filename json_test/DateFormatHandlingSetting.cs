﻿using Newtonsoft.Json;
using System;

namespace json_test
{
	/// <summary>
	/// This sample uses the DateFormatHandling setting to control how DateTime and DateTimeOffset are serialized.
	/// https://www.newtonsoft.com/json/help/html/SerializeDateFormatHandling.htm
	/// [20-04-2018]
	/// </summary>
	internal class DateFormatHandlingSetting
	{
		public DateFormatHandlingSetting()
		{
			DateTime mayanEndOfTheWorld = new DateTime(2012, 12, 21);

			string jsonIsoDate = JsonConvert.SerializeObject(mayanEndOfTheWorld);

			Console.WriteLine(jsonIsoDate);
			// "2012-12-21T00:00:00"

			string jsonMsDate = JsonConvert.SerializeObject(mayanEndOfTheWorld, new JsonSerializerSettings
			{
				DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
			});

			Console.WriteLine(jsonMsDate);
			// "\/Date(1356044400000+0100)\/"
		}
	}
}