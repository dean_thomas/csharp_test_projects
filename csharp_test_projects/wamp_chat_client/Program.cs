﻿using System;
using System.Threading.Tasks;
using WampSharp.V2;
using System.Linq;

namespace wamp_chat_client
{
	//	from js-console:
	//		connection.session.publish("com.wamp.chat", ["hello from JavaScript"]);

	enum Encoding
	{
		JSON,
		MSGPACK
	}

    class Program
    {
		const string PROMPT = ">>";

		protected DefaultWampChannelFactory channelFactory;
		protected IWampChannel channel;
		protected static string Url(string ip, uint port, bool secure = false) => $"{(secure ? "wss" : "ws")}://{ip}:{port}/ws";

		public Task StartSession(string ip, uint port, string realm, Encoding encoding = Encoding.JSON)
		{
			switch (encoding)
			{
				case Encoding.JSON:
					channel = channelFactory.CreateJsonChannel(Url(ip, port), realm);
					break;
				case Encoding.MSGPACK:
					channel = channelFactory.CreateMsgpackChannel(Url(ip, port), realm);
					break;
			}
			return channel.Open();
		}

		public void Subscribe(string topic = "com.wamp.chat")
		{
			channel.RealmProxy.Services.GetSubject(topic).Subscribe(
				next =>
				{
					var midRead = Console.CursorLeft > PROMPT.Length + 1;

					if (midRead)
						//	If we are mid input print an ellipsis at the end of the line...
						Console.WriteLine("...");
					else
						//	...else reset the cursor column...
						Console.SetCursorPosition(0, Console.CursorTop);

					//	...then print the message(s)...
					next.Arguments.ToList().ForEach(t => Console.WriteLine($"<< {t.Deserialize<string>()}"));

					//	...then print another input prompt (with ellipsis if we were mid-input already)
					Console.Write($"{PROMPT} {(midRead ? "..." : "")}");
				},
				error =>
				{
					Console.Error.WriteLine($"{error.Message}");
				});
		}

		public void SendMessage(string message, string topic = "com.wamp.chat")
		{
			var sub = channel.RealmProxy.Services.GetSubject<string>("com.example.publish.test");
			sub.OnNext(message);
		}

		protected Program()
		{
			channelFactory = new DefaultWampChannelFactory();
		}

		public string ReadFromConsole()
		{
			Console.Write($"{PROMPT} ");
			return Console.ReadLine();
		}

		static int Main(string[] args)
        {
			if (args.Length < 3)
			{
				Console.WriteLine("Usage:");
				Console.WriteLine("\twampchat ip port realm");
				return 0;
			}
			else
			{
				var ip = args[0];
				var port = uint.Parse(args[1]);
				var realm = args[2];

				var program = new Program();

				//	Try to connect (wait 5 seconds max)
				if (!program.StartSession(ip, port, realm).Wait(5000))
					return 1;

				Console.WriteLine($"Connected to Wamp session at {ip}:{port} on realm '{realm}'.");

				program.Subscribe();

				var line = "";
				while ((line = program.ReadFromConsole()) != ":q")
				{
					program.SendMessage(line);
				}
				return 0;
			}
		}
    }
}
