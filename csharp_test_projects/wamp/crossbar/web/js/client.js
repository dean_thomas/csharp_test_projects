console.log("Runnning on AutobahnJS ", autobahn.version);

// the URL of the WAMP Router (Crossbar.io)
//
var wsuri;
if (document.location.origin == "file://") {
    wsuri = "ws://127.0.0.1:8080/ws";

} else {
    wsuri = (document.location.protocol === "http:" ? "ws:" : "wss:") + "//" +
        document.location.host + "/ws";
}


// the WAMP connection to the Router
//
var connection = new autobahn.Connection({
    url: wsuri,
    realm: "realm1"
});


// fired when connection is established and session attached
//
connection.onopen = function (session, details) {

    console.log("Connected: ", session, details);

    var componentId = details.authid;
    var componentType = "JavaScript/Browser";

    console.log("Component ID is ", componentId);
    console.log("Component type is ", componentType);

    function on_publish_test(args) {
        var message = args[0];
        
        console.log("-----------------------");
        console.log("com.example.publish.test event, message: ", message);
    }

    session.subscribe('com.example.publish.test', on_publish_test).then(
        function (sub) {
            console.log("-----------------------");
            console.log('subscribed to topic "com.example.publish.test"');
        },
        function (err) {
            console.log("-----------------------");
            console.log('failed to subscribe to topic', err);
        }
    );

    function test1() {
        console.log("function 'test1' was called remotely");
    }
    session.register('com.example.call.test1', test1).then(
        function (sub) {
            console.log("-----------------------");
            console.log('registered RPC "com.example.call.test1"');
        },
        function (err) {
            console.log("-----------------------");
            console.log('failed to register RPC', err);
        }
    );

    function test2(args) {
        var result = 0;
        for (var i = 0; i < args[0].length; i++) 
        {
            result = result + parseInt(args[0][i]);        
        }
        console.log("function 'test2' was called remotely.  Arguments: " + args + ".  Result: "  + result);
        return result;
    }
    session.register('com.example.call.test2', test2).then(
        function (sub) {
            console.log("-----------------------");
            console.log('registered RPC "com.example.call.test2"');
        },
        function (err) {
            console.log("-----------------------");
            console.log('failed to register RPC', err);
        }
    );

    function test3(args) {
        var result = args[0] + args[1];
        
        console.log("function 'test3' was called remotely.  Arguments: " + args + ".  Result: "  + result);
        return result;
    }
    session.register('com.example.call.test3', test3).then(
        function (sub) {
            console.log("-----------------------");
            console.log('registered RPC "com.example.call.test3"');
        },
        function (err) {
            console.log("-----------------------");
            console.log('failed to register RPC', err);
        }
    );

    function test4(args) {
        var result = 0;
        
        console.log("function 'test4' was called remotely.  Arguments: " + args + ".  Result: "  + result);
        result = args[0] * args[0];
        return result;
    }
    session.register('com.example.call.test4', test4).then(
        function (sub) {
            console.log("-----------------------");
            console.log('registered RPC "com.example.call.test4"');
        },
        function (err) {
            console.log("-----------------------");
            console.log('failed to register RPC', err);
        }
    );
};


// fired when connection was lost (or could not be established)
//
connection.onclose = function (reason, details) {

    console.log("Connection lost: " + reason);

    clearInterval(t1);
    t1 = null;

    clearInterval(t2);
    t2 = null;

}


// now actually open the connection
//
connection.open();
