Crossbar server running on port 9000

To start:
    * From crossbar directory 
    ```bash
    $> crossbar start
    ```

Directory structure:
    * crossbar
        * web - contains html / json files for connecting via browser
        * .crossbar - contains configuration (curently with anonymous access on port 900)
