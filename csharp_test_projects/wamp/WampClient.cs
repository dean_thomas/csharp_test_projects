﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WampSharp.V2;
using WampSharp.V2.Client;

namespace wamp
{
	public class WampClient
	{
		static string Url(string ip, uint port) => $"ws://{ip}:{port}/ws";

		protected DefaultWampChannelFactory channelFactory;
		protected Dictionary<(string, uint, string), IWampChannel> channels = new Dictionary<(string, uint, string), IWampChannel>();

		public WampClient()
		{
			channelFactory = new DefaultWampChannelFactory();
		}

		public Task StartSession(string ip, uint port, string realm)
		{
			var channel = channelFactory.CreateJsonChannel(Url(ip, port), $"{realm}");
			channels.Add((ip, port, realm), channel);

			return channel.Open();
		}

		public IWampRealmProxy RealmProxy(string ip, uint port, string realm) => channels.GetValueOrDefault((ip, port, realm))?.RealmProxy;
	}
}
