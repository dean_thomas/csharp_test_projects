﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Linq;
using WampSharp.V2;
using WampSharp.V2.Client;
using WampSharp.V2.Core.Contracts;
using WampSharp.V2.PubSub;
using System.Collections.Generic;
using WampSharp.Core.Serialization;
using System.Timers;
using WampSharp.V2.Rpc;

namespace wamp
{
	class Program
    {
		static void SubscribeTest(string ip, uint port, string realm, WampClient wampClient)
		{
			void WampSubjectSubscribe()
			{
				//	https://wampsharp.net/wamp2/roles/subscriber/getting-started-with-subscriber/
				//	[26-08-2018]
				var sub = wampClient.RealmProxy(ip, port, realm).Services.GetSubject("com.example.oncounter").Subscribe(
				next =>
				{
					Console.WriteLine($"Got event {next}");
				},
				error =>
				{
					Console.Error.WriteLine($"Got error {error}");
				});
			}

			void WampRawSubscribe()
			{
				//	https://wampsharp.net/wamp2/roles/subscriber/raw-subscriber/
				//	[26-08-2018]
				wampClient.RealmProxy(ip, port, realm).TopicContainer.GetTopicByUri("com.example.oncounter").Subscribe(new WamplLowLevelLoggingObserver(), new SubscribeOptions());
			}

			void WampObserverSubscribe()
			{
				var sub = wampClient.RealmProxy(ip, port, realm).Services.GetSubject("com.example.oncounter").Subscribe(new WampLoggingObserver());
			}

			//	The WampSubject is the easiest way to subscribe to a topic of a WAMP router.
			WampSubjectSubscribe();

			//	The 'raw' interface allows event messages to be handled as required
			WampRawSubscribe();

			//	It is also possible to subsribe using an observer object
			WampObserverSubscribe();
		}


		static void PublishTest(string ip, uint port, string realm, WampClient wampClient)
		{
			void WampSubjectPublish()
			{
				//	https://wampsharp.net/wamp2/roles/publisher/getting-started-with-publisher/
				//	[26-08-2018]
				Console.WriteLine("publishing to 'com.example.publish.test'");
				var sub = wampClient.RealmProxy(ip, port, realm).Services.GetSubject<string>("com.example.publish.test");
				sub.OnNext("Hello from C#");
			}

			async void WampRawPublish()
			{
				//	https://wampsharp.net/wamp2/roles/publisher/raw-publisher/
				//	[26-08-2018]
				var topic = wampClient.RealmProxy(ip, port, realm).TopicContainer.GetTopicByUri("com.example.publish.test");
				Task<long?> publishTask = topic.Publish(new PublishOptions
				{
					Acknowledge = true,
					DiscloseMe = true
				}, new object[] { "Hello from C# (raw interface)" });

				try
				{
					var pubId = await publishTask;

					if (pubId != null)
					{
						Console.WriteLine($"Event published with publication ID {pubId}");
					}
				}
				catch (WampException ex)
				{
					Console.WriteLine($"An error occured while publishing event: {ex}");
				}
			}

			var timer = new Timer(3000);
			timer.Elapsed += (sender, args) =>
			{
				//	Using the WampSubject interface is the easiest way to publish
				WampSubjectPublish();

				//	Raw publisher method allows us to get feedback from publications
				WampRawPublish();
			};

			timer.Start();
		}


		static void CallTest(string ip, uint port, string realm, WampClient wampClient)
		{
			void WampCallProxy()
			{
				var proxy = wampClient.RealmProxy(ip, port, realm).Services.GetCalleeProxy<IArgumentsService>();
				proxy.Test1();
				Console.WriteLine("Called Test1");
				var r2 = proxy.Test2(new List<int> { 1, 2, 3, 4, 5 });
				Console.WriteLine($"Called Test2 - result {r2}");
				var r3 = proxy.Test3(5, 6);
				Console.WriteLine($"Called Test3 - result {r3}");
				var r4 = proxy.Test4(3);
				Console.WriteLine($"Called Test4 - result {r4}");
			}
			

			var timer = new Timer(1000);
			timer.Elapsed += (sender, args) =>
			{
				//	The Callee proxy is the easiest way to call rpc methods of a WAMP router.
				WampCallProxy();
			};
			timer.Start();
		}

		static void Main(string[] args)
        {
			Debug.Assert(args.Length == 3);

			var ip = args[0];
			var port = uint.Parse(args[1]);
			var realm = args[2];

			Console.WriteLine($"Trying to connect to Wamp Router at {ip}:{port} on realm '{realm}'");

			var wampClient = new WampClient();

			var openTask = wampClient.StartSession(ip, port, realm);
			try
			{
				//	Try and connect
				openTask.Wait();
				Console.WriteLine($"Connected to Wamp Router at {ip}:{port} on realm '{realm}'");

				//SubscribeTest(ip, port, realm, wampClient);
				//PublishTest(ip, port, realm, wampClient);
				CallTest(ip, port, realm, wampClient);
			}
			catch (Exception ex)
			{
				Console.Error.WriteLine(ex.Message);
			}


			Console.WriteLine("Press enter to exit.");
			Console.ReadLine();
		}


	}
}
