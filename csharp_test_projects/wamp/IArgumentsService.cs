﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WampSharp.V2.Rpc;

namespace wamp
{
	public interface IArgumentsService
	{
		[WampProcedure("com.example.call.test1")]
		void Test1();

		[WampProcedure("com.example.call.test2")]
		int Test2(List<int> values);

		[WampProcedure("com.example.call.test3")]
		int Test3(int a, int b);

		[WampProcedure("com.example.call.test4")]
		int Test4(int a);
	}
}
