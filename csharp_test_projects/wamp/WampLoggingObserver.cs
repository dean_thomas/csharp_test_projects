﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WampSharp.V2;

namespace wamp
{
	public class WampLoggingObserver : IObserver<IWampSerializedEvent>
	{
		public void OnCompleted()
		{
			Console.WriteLine($"Completed.");
		}

		public void OnError(Exception error)
		{
			Console.Error.WriteLine($"{error}");
		}

		public void OnNext(IWampSerializedEvent value)
		{
			Console.WriteLine($"Received message on topic '{value.Details.Topic}' with id: {value.PublicationId}.");
			Console.WriteLine($"Arguments: ");
			value?.Arguments?.ToList()?.ForEach(t => Console.WriteLine($"\t{t?.Deserialize<object>() ?? "<null>"}"));
			Console.WriteLine($"Named Arguments: ");
			value?.ArgumentsKeywords?.ToList()?.ForEach(t => Console.WriteLine($"\t{t.Key}: {t.Value?.Deserialize<object>() ?? "<null>"}"));
		}
	}
}
