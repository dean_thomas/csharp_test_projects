﻿using System;
using System.Collections.Generic;
using System.Text;
using WampSharp.Core.Serialization;
using WampSharp.V2.Core.Contracts;
using WampSharp.V2.PubSub;

namespace wamp
{
	public class WamplLowLevelLoggingObserver : IWampRawTopicClientSubscriber
	{
		public void Event<TMessage>(IWampFormatter<TMessage> formatter, long publicationId, EventDetails details)
		{
			Console.WriteLine($"{publicationId}");
		}

		public void Event<TMessage>(IWampFormatter<TMessage> formatter, long publicationId, EventDetails details, TMessage[] arguments)
		{
			Console.WriteLine($"{publicationId}");
		}

		public void Event<TMessage>(IWampFormatter<TMessage> formatter, long publicationId, EventDetails details, TMessage[] arguments, IDictionary<string, TMessage> argumentsKeywords)
		{
			Console.WriteLine($"{publicationId}");
		}
	}
}
